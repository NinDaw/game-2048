
import gamefield.ui.MainView2Controller;
import gamefield.ui.MainViewController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by RENT on 2017-07-18.
 */
public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("gamefield/ui/MainView2.fxml"));
        VBox root = fxmlLoader.load();
        MainView2Controller controller = fxmlLoader.getController();
        controller.initialize(primaryStage);
        primaryStage.setTitle("2048");
        primaryStage.setScene(new Scene(root, 602, 500));
        primaryStage.show();
    }

    public static void main(String[] args) {

        launch(args);

    }
}
