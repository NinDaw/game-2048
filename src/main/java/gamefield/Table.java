package gamefield;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by RENT on 2017-07-18.
 */
public class Table {
    private Integer[][] mainTable;


    public Table() {
        this.mainTable = new Integer[4][4];
    }

    public Integer[][] getMainTable() {
        return mainTable;
    }

    public void moveLeft() {
        for (int x = 1; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                moveNumbersLeft(x, y);
            }
        }
    }

    public void moveRight() {
        for (int x = 2; x >= 0; x--) {
            for (int y = 0; y < 4; y++) {
                moveNumbersRight(x, y);
            }
        }
    }

    public void moveDown() {
        for (int y = 2; y >= 0; y--) {
            for (int x = 0; x < 4; x++) {
                moveNumbersDown(x, y);
            }
        }
    }

    public void moveUp() {
        for (int y = 1; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                moveNumbersUp(x, y);
            }
        }
    }

    public void addNumber() {
        if(isAnyNull()) {
            Random random = new Random();
            int x = random.nextInt(4);
            int y = random.nextInt(4);
            Integer a = mainTable[x][y];
           while(a!=null){
               x = random.nextInt(4);
               y = random.nextInt(4);
               a = mainTable[x][y];
           }
           if(random.nextInt(8)<7){
                mainTable[x][y] = 2;
            }else{
                mainTable[x][y] = 4;
            }
        }else{
            System.out.println("KONIEC GRY!!!");
        }
    }

    private boolean isAnyNull(){
        boolean isNull = false;
        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                if(mainTable[x][y]==null){
                    isNull = true;
                    break;
                }
            }
        }
        return isNull;
    }

    private Integer findASpot(){
        Random random = new Random();
        int x = random.nextInt(4);
        int y = random.nextInt(4);
        return mainTable[x][y];
    }



    private void moveNumbersLeft(int x, int y) {
        if (numberFromLeft(x, y) == null) {
            if (x == 1) {
                mainTable[x - 1][y] = mainTable[x][y];
                mainTable[x][y] = null;
            } else {
                mainTable[x - 1][y] = mainTable[x][y];
                mainTable[x][y] = null;
                x--;
                moveNumbersLeft(x, y);
            }
        } else if (numberFromLeft(x, y) == mainTable[x][y]) {
            mainTable[x - 1][y] += mainTable[x][y];
            mainTable[x][y] = null;
        }
    }

    private void moveNumbersUp(int x, int y) {
        if (numberFromUp(x, y) == null) {
            if (y == 1) {
                mainTable[x][y - 1] = mainTable[x][y];
                mainTable[x][y] = null;
            } else {
                mainTable[x][y - 1] = mainTable[x][y];
                mainTable[x][y] = null;
                y--;
                moveNumbersUp(x, y);
            }
        } else if (numberFromUp(x, y) == mainTable[x][y]) {
            mainTable[x][y - 1] += mainTable[x][y];
            mainTable[x][y] = null;
        }
    }

    private void moveNumbersRight(int x, int y) {
        if (numberFromRight(x, y) == null) {
            if (x == 2) {
                mainTable[x + 1][y] = mainTable[x][y];
                mainTable[x][y] = null;
            } else {
                mainTable[x + 1][y] = mainTable[x][y];
                mainTable[x][y] = null;
                x++;
                moveNumbersRight(x, y);
            }
        } else if (numberFromRight(x, y) == mainTable[x][y]) {
            mainTable[x + 1][y] += mainTable[x][y];
            mainTable[x][y] = null;
        }
    }

    private void moveNumbersDown(int x, int y) {
        if (numberFromDown(x, y) == null) {
            if (y == 2) {
                mainTable[x][y + 1] = mainTable[x][y];
                mainTable[x][y] = null;
            } else {
                mainTable[x][y + 1] = mainTable[x][y];
                mainTable[x][y] = null;
                y++;
                moveNumbersDown(x, y);
            }
        } else if (numberFromDown(x, y) == mainTable[x][y]) {
            mainTable[x][y + 1] += mainTable[x][y];
            mainTable[x][y] = null;
        }
    }

    private Integer numberFromLeft(int x, int y) {
        return mainTable[x - 1][y];
    }

    private Integer numberFromRight(int x, int y) {
        return mainTable[x + 1][y];
    }

    private Integer numberFromUp(int x, int y) {
        return mainTable[x][y - 1];
    }

    private Integer numberFromDown(int x, int y) {
        return mainTable[x][y + 1];
    }

//    public void addNumber(int value, int x, int y){
//        Number number = new Number(value, x, y);
//        this.getMainTable()[x][y] = number;
//    }

    @Override
    public String toString() {
        String result = "";

        for (int y = 0; y < 4; y++) {
            result += "\n";
            for (int x = 0; x < 4; x++) {
                result += ("[" + mainTable[x][y] + "]");
            }
        }
        return result;
    }
}