package gamefield.ui;

import gamefield.Table;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by RENT on 2017-07-18.
 */
public class MainViewController {
    @FXML
    VBox vBox;
    @FXML
    Label label11;
    @FXML
    Label label12;
    @FXML
    Label label13;
    @FXML
    Label label14;
    @FXML
    Label label21;
    @FXML
    Label label22;
    @FXML
    Label label23;
    @FXML
    Label label24;
    @FXML
    Label label31;
    @FXML
    Label label32;
    @FXML
    Label label33;
    @FXML
    Label label34;
    @FXML
    Label label41;
    @FXML
    Label label42;
    @FXML
    Label label43;
    @FXML
    Label label44;
    @FXML
    Button btnUP;
    @FXML
    Button btnLEFT;
    @FXML
    Button btnRIGHT;
    @FXML
    Button btnDOWN;

    private Table table;

    public void initialize(Stage stage){
        btnDOWN.setOnAction(event -> {
            table.moveDown();
            table.addNumber();
            refreshTable();
        });
        btnUP.setOnAction(event -> {
            table.moveUp();
            table.addNumber();
            refreshTable();
        });
        btnLEFT.setOnAction(event -> {
            table.moveLeft();
            table.addNumber();
            refreshTable();
        });
        btnRIGHT.setOnAction(event -> {
            table.moveRight();
            table.addNumber();
            refreshTable();
        });
        table = new Table();
        table.getMainTable()[0][3]=2;
        table.getMainTable()[1][3]=2;
        table.getMainTable()[2][3]=4;
        table.getMainTable()[1][2]=2;
        table.getMainTable()[3][3]=4;

        refreshTable();

        vBox.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case UP:
                   table.moveUp();
                    table.addNumber();
                   refreshTable();
                    break;
                case DOWN:
                    table.moveDown();
                    table.addNumber();
                    refreshTable();
                    break;
                case LEFT:
                    table.moveLeft();
                    table.addNumber();
                    refreshTable();
                    break;
                case RIGHT:
                    table.moveRight();
                    table.addNumber();
                    refreshTable();

                    break;
            }
        });


    }

    private void refreshTable() {
        label11.setText(getNumber(0,0));
        label12.setText(getNumber(1,0));
        label13.setText(getNumber(2,0));
        label14.setText(getNumber(3,0));

        label21.setText(getNumber(0,1));
        label22.setText(getNumber(1,1));
        label23.setText(getNumber(2,1));
        label24.setText(getNumber(3,1));

        label31.setText(getNumber(0,2));
        label32.setText(getNumber(1,2));
        label33.setText(getNumber(2,2));
        label34.setText(getNumber(3,2));

        label41.setText(getNumber(0,3));
        label42.setText(getNumber(1,3));
        label43.setText(getNumber(2,3));
        label44.setText(getNumber(3,3));
    }

    private String getNumber(int x, int y){
        String value = null;
        Integer a = table.getMainTable()[x][y];
      if (a != null){
            value = String.valueOf(a);
        }
        else{
          value = "[]";
      }
      return value;
    }
}
