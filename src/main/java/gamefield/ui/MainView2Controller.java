package gamefield.ui;

import gamefield.Table;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by RENT on 2017-08-07.
 */
public class MainView2Controller {
    Image image2 = new Image(getClass().getResource("two.png").toExternalForm());
    Image image0 = new Image(getClass().getResource("0.png").toExternalForm());
    Image image4 = new Image(getClass().getResource("4.png").toExternalForm());
    Image image8 = new Image(getClass().getResource("8.png").toExternalForm());
    Image image16 = new Image(getClass().getResource("16.png").toExternalForm());
    Image image32 = new Image(getClass().getResource("32.png").toExternalForm());
    Image image64 = new Image(getClass().getResource("64.png").toExternalForm());
    Image image128 = new Image(getClass().getResource("128.png").toExternalForm());
    Image image256 = new Image(getClass().getResource("256.png").toExternalForm());
    Image image512 = new Image(getClass().getResource("512.png").toExternalForm());
    Image image1024 = new Image(getClass().getResource("1024.png").toExternalForm());
    Image image2048 = new Image(getClass().getResource("2048.png").toExternalForm());
    @FXML
    VBox vBox;
    @FXML
    ImageView img11;
    @FXML
    ImageView img12;
    @FXML
    ImageView img13;
    @FXML
    ImageView img14;

    @FXML
    ImageView img21;
    @FXML
    ImageView img22;
    @FXML
    ImageView img23;
    @FXML
    ImageView img24;

    @FXML
    ImageView img31;
    @FXML
    ImageView img32;
    @FXML
    ImageView img33;
    @FXML
    ImageView img34;

    @FXML
    ImageView img41;
    @FXML
    ImageView img42;
    @FXML
    ImageView img43;
    @FXML
    ImageView img44;

    @FXML
    Button btnUP;
    @FXML
    Button btnLEFT;
    @FXML
    Button btnRIGHT;
    @FXML
    Button btnDOWN;

    private Table table;

    public void initialize(Stage stage) {
        btnDOWN.setOnAction(event -> {
            table.moveDown();
            table.addNumber();
            refreshTable();
        });
        btnUP.setOnAction(event -> {
            table.moveUp();
            table.addNumber();
            refreshTable();
        });
        btnLEFT.setOnAction(event -> {
            table.moveLeft();
            table.addNumber();
            refreshTable();
        });
        btnRIGHT.setOnAction(event -> {
            table.moveRight();
            table.addNumber();
            refreshTable();
        });
        table = new Table();
        table.getMainTable()[0][3] = 2;
        table.getMainTable()[1][3] = 2;
        table.getMainTable()[2][3] = 4;
        table.getMainTable()[1][2] = 2;
        table.getMainTable()[3][3] = 4;

        refreshTable();

        vBox.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case UP:
                    table.moveUp();
                    table.addNumber();
                    refreshTable();
                    break;
                case DOWN:
                    table.moveDown();
                    table.addNumber();
                    refreshTable();
                    break;
                case LEFT:
                    table.moveLeft();
                    table.addNumber();
                    refreshTable();
                    break;
                case RIGHT:
                    table.moveRight();
                    table.addNumber();
                    refreshTable();

                    break;
            }
        });


    }

    private void refreshTable() {
        img11.setImage(getAPic(0, 0));
        img12.setImage(getAPic(1, 0));
        img13.setImage(getAPic(2, 0));
        img14.setImage(getAPic(3, 0));

        img21.setImage(getAPic(0, 1));
        img22.setImage(getAPic(1, 1));
        img23.setImage(getAPic(2, 1));
        img24.setImage(getAPic(3, 1));

        img31.setImage(getAPic(0, 2));
        img32.setImage(getAPic(1, 2));
        img33.setImage(getAPic(2, 2));
        img34.setImage(getAPic(3, 2));

        img41.setImage(getAPic(0, 3));
        img42.setImage(getAPic(1, 3));
        img43.setImage(getAPic(2, 3));
        img44.setImage(getAPic(3, 3));

    }

    private Integer getNumber(int x, int y) {
        Integer value = null;
        Integer a = table.getMainTable()[x][y];
        if (a != null) {
            value = a;
        } else {
            value = null;
        }
        return value;
    }

    private Image getAPic(int x, int y) {
        Integer a = getNumber(x, y);
        Image image = null;
        if (a != null) {
            switch (Integer.valueOf(a)) {

                case 2:
                    image = image2;
                    break;
                case 4:
                    image = image4;
                    break;
                case 8:
                    image = image8;
                    break;
                case 16:
                    image = image16;
                    break;
                case 32:
                    image = image32;
                    break;
                case 64:
                    image = image64;
                    break;
                case 128:
                    image = image128;
                    break;
                case 256:
                    image = image256;
                    break;
                case 512:
                    image = image512;
                    break;
                case 1024:
                    image = image1024;
                    break;
                case 2048:
                    image = image2048;
                    break;
            }
        } else {
            image = image0;
        }
        return image;
    }
}


