package gamefield;

/**
 * Created by RENT on 2017-07-18.
 */
public class Number {
    private int value;
    private int x;
    private int y;

    public Number(int value, int x, int y) {
        this.value = value;
        this.x = x;
        this.y = y;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    @Override
    public String toString() {
        return String.valueOf(getValue());
    }

}
